import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClockComponent } from './clock/clock.component';

@NgModule({
	declarations: [AppComponent, ClockComponent],

	imports: [
		BrowserModule,
		CommonModule,
		AppRoutingModule,

		ServiceWorkerModule.register('ngsw-worker.js', {
			enabled: environment.production
		})
	],

	providers: [],

	bootstrap: [AppComponent]
})
export class AppModule {}

import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { DateTime } from 'luxon';

import { ScheduleService } from 'app/services/schedule.service';

@Component({
	selector: 'app-clock',
	templateUrl: './clock.component.html',
	styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {
	readonly time$: Observable<string> = this.scheduler.tick$.pipe(
		map((t: DateTime) => t.toISOTime())
	);

	constructor(private scheduler: ScheduleService) {}

	ngOnInit() {}
}

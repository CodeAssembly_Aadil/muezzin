import { Injectable } from '@angular/core';

import { interval as Interval, Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';

import { DateTime } from 'luxon';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  readonly tick$: Observable<DateTime>;

  constructor() {
	  this.tick$ = Interval(1000).pipe(map((tick) => DateTime.local()), share());
  }
}
